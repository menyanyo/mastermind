from rest_framework import viewsets
from rest_framework.response import Response

from Api.serializers import CreateGameSerializer, AttemptCodebreakerSerializer



class CreateGameViewSet(viewsets.ViewSet):
    """
    API endpoint that allows you to create games. Using ModelViewSet for creating games faster.
    """

    http_method_names = ['post']
    serializer_class = CreateGameSerializer

    def create(self, request):
        serializer = CreateGameSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "error",
                "message": "Invalid fields",
                "details": serializer.errors,
            })
        return serializer.save()


class AttemptCodebreaker(viewsets.ViewSet):
    """
    API endpoint that allows you to create games. Using ModelViewSet for creating games faster.
    """

    http_method_names = ['post']
    serializer_class = AttemptCodebreakerSerializer

    def create(self, request):
        serializer = AttemptCodebreakerSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "error",
                "message": "Invalid fields",
                "details": serializer.errors,
            })
        return serializer.save()
