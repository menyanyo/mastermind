from rest_framework import serializers
from rest_framework.response import Response
from django.contrib.auth.models import User

from Pattern.serializers import PatternSerializer

from Game.models import Game
from Pattern.models import Pattern

from Workflow.pegs import codemaker_winner, get_pegs


# Create Game serializer
class CreateGameSerializer(serializers.Serializer):
    """Serializer required for the api call creategame"""

    codemaker = serializers.CharField()
    pattern = PatternSerializer()

    @staticmethod
    def validate_codemaker(name):
        """Create the user if he/she does not exists yet. Make sure that is only 1 game opened for each user"""

        if not User.objects.filter(username=name).exists():
            User.objects.create(username=name)
        if Game.objects.filter(codemaker__username=name, status="opened").exists():
            raise serializers.ValidationError("Game already open for this user")
        return name

    @staticmethod
    def validate_pattern(pattern_data):
        """Create the pattern combination in the db if it does not exists"""

        if not Pattern.objects.filter(**pattern_data).exists():
            Pattern.objects.create(**pattern_data)
        return pattern_data

    def save(self):
        """Create the game with the validated_data"""

        codemaker = User.objects.get(username=self.validated_data['codemaker'])
        pattern = Pattern.objects.get(**self.validated_data['pattern'])
        Game.objects.create(
            codemaker=codemaker,
            pattern=pattern,
            status='opened',
        )
        return Response({"status": "success", "message": "Successfully created", "codemaker": codemaker.username})


# Create Game serializer
class AttemptCodebreakerSerializer(serializers.Serializer):
    """Serializer required for the api call attemptcodebreaker"""

    codemaker = serializers.CharField()
    pattern = PatternSerializer()

    @staticmethod
    def validate_codemaker(name):
        """Make sure if there is a game for the given user"""

        if not Game.objects.filter(codemaker__username=name, status="opened").exists():
            raise serializers.ValidationError("There is no opened game for this user")
        return name

    @staticmethod
    def validate_pattern(pattern_data):
        """Create the pattern combination in the db if it does not exists"""
        if not Pattern.objects.filter(**pattern_data).exists():
            Pattern.objects.create(**pattern_data)
        return pattern_data

    def save(self):
        """Get the pegs dict and return the response, giving the info about if the codebreaker won or not"""

        codemaker = User.objects.get(username=self.validated_data['codemaker'])
        pattern = Pattern.objects.get(**self.validated_data['pattern'])
        game = Game.objects.get(status="opened", codemaker=codemaker)

        # Response
        pegs_dict = get_pegs(game.pattern, pattern)
        win = codemaker_winner(pegs_dict)
        if win:
            game.status = "closed"
            game.save()
        return Response({"status": "success", "win": str(win), **pegs_dict})
