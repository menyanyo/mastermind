import random

from rest_framework.test import APITestCase
from rest_framework import status

from django.contrib.auth.models import User
from django.urls import reverse

from Pattern.models import Pattern
from Mastermind.constants import PATTERN_COLOURS
from Game.models import Game

random_user_name = "usertest_{}".format(hash(random.random()))
list_colours = [colour_tuple[0] for colour_tuple in PATTERN_COLOURS]


class ApiCreateGameTestCase(APITestCase):

    def setUp(self):
        """We create an existing game for testing purposes"""

        user = User.objects.create(username=random_user_name)
        pattern = Pattern.set_code(list_colours)
        pattern.save()
        Game.objects.create(
            codemaker=user,
            pattern=pattern,
            status='opened',
        )

    def test_create_game(self):
        """Testing the creation of a game"""

        url = reverse('api:creategame-list')
        data = {
            "codemaker": "marc",
            "pattern": {
                'firstColour': 'red',
                'secondColour': 'red',
                'thirdColour': 'blue',
                'fourthColour': 'blue',
            }
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(data['status'], 'success')
        self.assertEqual(data['codemaker'], 'marc')
        new_game = Game.objects.filter(
            codemaker__username='marc',
            status='opened'
        )
        self.assertTrue(new_game.exists())

    def test_avoid_duplicated_games(self):
        """Testing if we avoid to create 2 games for the same user"""

        url = reverse('api:creategame-list')
        data = {
            "codemaker": random_user_name,
            "pattern": {
                'firstColour': list_colours[0],
                'secondColour': list_colours[1],
                'thirdColour': list_colours[2],
                'fourthColour': list_colours[3],
            }
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(data['status'], 'error')
        games = Game.objects.filter(
            codemaker__username=random_user_name,
            status='opened'
        )
        self.assertNotEqual(games.count(), 2)


class ApiAttemptCodebreakerTestCase(APITestCase):

    def setUp(self):
        """We create a game for testing purposes"""

        user = User.objects.create(username=random_user_name)
        pattern = Pattern.set_code(list_colours)
        pattern.save()
        Game.objects.create(
            codemaker=user,
            pattern=pattern,
            status='opened',
        )

    def test_win(self):
        """Check if the integrated workflow works fine. If the pattern is the right, pegs_colourPosition has to be 4.
        Win has to be True and all the games for the codemaker must be closed"""

        url = reverse('api:attemptcodebreaker-list')
        data = {
            "codemaker": random_user_name,
            "pattern": {
                'firstColour': list_colours[0],
                'secondColour': list_colours[1],
                'thirdColour': list_colours[2],
                'fourthColour': list_colours[3],
            }
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(data['status'], 'success')
        self.assertEqual(data['pegs_colourPosition'], 4)
        self.assertEqual(data['pegs_colour'], 0)
        self.assertEqual(data['win'], 'True')

        opened_game = Game.objects.filter(
            codemaker__username=random_user_name,
            status='opened'
        )
        self.assertFalse(opened_game.exists())

    def test_lose(self):
        """Check if the integrated workflow works fine. The pattern given makes: 2 peg with colour and position and
        2 pegs only colour. Win has to be False and the game must be opened"""

        url = reverse('api:attemptcodebreaker-list')
        data = {
            "codemaker": random_user_name,
            "pattern": {
                'firstColour': list_colours[0],
                'secondColour': list_colours[2],
                'thirdColour': list_colours[1],
                'fourthColour': list_colours[3],
            }
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(data['status'], 'success')
        self.assertEqual(data['pegs_colourPosition'], 2)
        self.assertEqual(data['pegs_colour'], 2)
        self.assertEqual(data['win'], 'False')

        games = Game.objects.filter(
            codemaker__username=random_user_name,
            status='opened'
        )
        self.assertEqual(games.count(), 1)
