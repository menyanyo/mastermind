from django.conf.urls import url, include
from rest_framework import routers

from Api.views import CreateGameViewSet, AttemptCodebreaker

router = routers.DefaultRouter()
router.register(r'creategame', CreateGameViewSet, basename="creategame")
router.register(r'attemptcodebreaker', AttemptCodebreaker, basename="attemptcodebreaker")


urlpatterns = [
    url(r'^', include(router.urls))
]
