from rest_framework import serializers

from Game.models import Game


# Game serializer
class GameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Game
        fields = ('id', 'codemaker', 'pattern', 'status')
