from django.db import models
from django.contrib.auth.models import User

from Mastermind.constants import GAME_STATUS
from Pattern.models import Pattern


class Game(models.Model):

    codemaker = models.ForeignKey(User, on_delete=models.CASCADE)
    pattern = models.ForeignKey(Pattern, on_delete=models.PROTECT)
    status = models.CharField(max_length=10, choices=GAME_STATUS)

    def __str__(self):
        return u"Game -> {}".format(self.codemaker)
