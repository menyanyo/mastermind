from rest_framework import viewsets, response, status

from Game.serializers import GameSerializer
from Game.models import Game


class GameViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to create games.
    """
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    http_method_names = ['get']
