from django.db import models

from Mastermind.constants import PATTERN_COLOURS, NUMBER_COLOURS_PATTERN, MAX_REPEATED_COLOUR


class Pattern(models.Model):

    firstColour = models.CharField(max_length=10, choices=PATTERN_COLOURS)
    secondColour = models.CharField(max_length=10, choices=PATTERN_COLOURS)
    thirdColour = models.CharField(max_length=10, choices=PATTERN_COLOURS)
    fourthColour = models.CharField(max_length=10, choices=PATTERN_COLOURS)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['firstColour', 'secondColour', 'thirdColour', 'fourthColour'],
                name='pattern_duplicated',
            )
        ]

    def __str__(self):
        return ', '.join(self.get_code())

    def is_valid(self):
        """Check if there is some colour repeated more than 2 times"""

        list_colours = [tuple[0] for tuple in PATTERN_COLOURS]
        pattern = self.get_code()
        for colour in pattern:
            if colour not in list_colours or pattern.count(colour) > MAX_REPEATED_COLOUR:
                return False
        return True

    def get_code(self):
        """Return a sorted array with the colours of a pattern"""

        return [
            self.firstColour,
            self.secondColour,
            self.thirdColour,
            self.fourthColour,
        ]

    @classmethod
    def set_code(cls, pattern):
        return cls(
            firstColour=pattern[0],
            secondColour=pattern[1],
            thirdColour=pattern[2],
            fourthColour=pattern[3],
        )


class Pegs(models.Model):
    class Meta:
        managed = False  # We do not need to save pegs in the db

    pegs_colourPosition = models.PositiveSmallIntegerField()
    pegs_colour = models.PositiveSmallIntegerField()

    def is_valid(self):
        """Check if there is some colour repeated more than 2 times"""

        if self.pegs_colourPosition + self.pegs_colour > NUMBER_COLOURS_PATTERN:
            return False
        elif self.pegs_colourPosition > NUMBER_COLOURS_PATTERN or self.pegs_colour > NUMBER_COLOURS_PATTERN:
            return False
        else:
            return True

    def get_dict(self):
        """Return a dict with every type of pig and the number of it"""

        return {
            "pegs_colourPosition": self.pegs_colourPosition,
            "pegs_colour": self.pegs_colour,
        }

    @classmethod
    def set_dict(cls, dic):
        """Return a dict with every type of pig and the number of it"""

        return cls(
            pegs_colourPosition=dic["pegs_colourPosition"],
            pegs_colour=dic["pegs_colour"],
        )
