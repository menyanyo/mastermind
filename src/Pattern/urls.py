from django.conf.urls import url, include
from rest_framework import routers

from Pattern.views import PatternViewSet


router = routers.DefaultRouter()
router.register(r'pattern', PatternViewSet, basename='pattern')


urlpatterns = [
    url(r'^', include(router.urls))
]
