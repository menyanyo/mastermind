from django.test import TestCase

from django.db.utils import IntegrityError

from Pattern.models import Pattern, Pegs
from Mastermind.constants import PATTERN_COLOURS


list_colours = [colour_tuple[0] for colour_tuple in PATTERN_COLOURS]


class PatternTestCase(TestCase):

    def setUp(self):
        Pattern.set_code(list_colours).save()

    def test_cant_create_duplicated_patterns(self):
        """Do not allow create duplicated patterns"""
        try:
            Pattern.set_code([colour_tuple[0] for colour_tuple in PATTERN_COLOURS]).save()
        except IntegrityError:
            pass
        else:
            self.fail("The object has been duplicated correctly")

    def test_pattern_validation(self):
        """Test if the method is_valid works for the Patterns"""

        invalid_pattern = Pattern.set_code([
            'red',
            'red',
            'red',
            'blue'
        ])
        correct_pattern = Pattern.set_code([
            'red',
            'red',
            'blue',
            'green'
        ])
        self.assertFalse(invalid_pattern.is_valid())
        self.assertTrue(correct_pattern.is_valid())


class PegsTestCase(TestCase):

    def test_pegs_validation(self):
        """Test if the method is_valid works for the Pegs"""

        invalid_pegs = Pegs(
            pegs_colourPosition=1,
            pegs_colour=4,
        )
        correct_pegs = Pegs(
            pegs_colourPosition=3,
            pegs_colour=1,
        )
        self.assertFalse(invalid_pegs.is_valid())
        self.assertTrue(correct_pegs.is_valid())
