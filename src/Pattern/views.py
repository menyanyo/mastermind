from rest_framework import viewsets

from Pattern.models import Pattern
from Pattern.serializers import PatternSerializer


class PatternViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows pattern be viewed
    """

    queryset = Pattern.objects.all()
    serializer_class = PatternSerializer
    http_method_names = ['get']
