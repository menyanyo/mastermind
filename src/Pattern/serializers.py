from rest_framework import serializers

from Pattern.models import Pattern


# Game serializer
class PatternSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pattern
        fields = ('firstColour', 'secondColour', 'thirdColour', 'fourthColour')

    def validate(self, attrs):
        pattern = Pattern(**attrs)
        if not pattern.is_valid():
            raise serializers.ValidationError("Pattern out of the rules")
        return attrs
