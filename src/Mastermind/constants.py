
NUMBER_COLOURS_PATTERN = 4

MAX_REPEATED_COLOUR = 2

PATTERN_COLOURS = [
    ("yellow", "Yellow"),
    ("green", "Green"),
    ("blue", "Blue"),
    ("red", "Red"),
]

GAME_STATUS = [
    ('opened', 'OPENED'),
    ('closed', 'CLOSED'),
]
