from django.test import TestCase

from Pattern.models import Pattern
from Mastermind.constants import PATTERN_COLOURS
from Workflow.pegs import get_pegs, codemaker_winner


list_colours = [colour_tuple[0] for colour_tuple in PATTERN_COLOURS]


class WorkflowPegsTestCase(TestCase):

    def test_get_pegs(self):
        """Check if the function is returning the right response"""

        codemaker_pattern = Pattern.set_code([
            'red', 'yellow', 'blue', 'red',
        ])
        codebreaker_pattern = Pattern.set_code([
            'yellow', 'yellow', 'blue', 'blue',
        ])
        result = get_pegs(codemaker_pattern, codebreaker_pattern)
        self.assertEqual(
            result['pegs_colourPosition'], 2
        )
        self.assertEqual(
            result['pegs_colour'], 0
        )

    def test_codemaker_winner(self):
        # Test a non win move
        codemaker_pattern = Pattern.set_code([
            'red', 'yellow', 'yellow', 'red',
        ])
        codebreaker_pattern = Pattern.set_code([
            'yellow', 'yellow', 'blue', 'blue',
        ])
        pegs_dict = get_pegs(codemaker_pattern, codebreaker_pattern)
        win = codemaker_winner(pegs_dict)
        self.assertFalse(win)

        # Test a win move
        codemaker_pattern = Pattern.set_code([
            'red', 'yellow', 'yellow', 'red',
        ])
        codebreaker_pattern = Pattern.set_code([
            'red', 'yellow', 'yellow', 'red',
        ])
        pegs_dict = get_pegs(codemaker_pattern, codebreaker_pattern)
        win = codemaker_winner(pegs_dict)
        self.assertTrue(win)
