# BASE GAME UTILS, LOGIC AND WORKFLOW ABOUT PEGS

from Mastermind.constants import PATTERN_COLOURS


def get_pegs(makercode, breakercode):
    """Receive a the codemaker pattern and a breakercode and returns
    a dictionary with the black pegs (colour and position matched)
    and white pegs (only colour matched)"""

    remaining_maker, remaining_breaker = [], []
    dict_response = {"pegs_colourPosition": 0, "pegs_colour": 0}
    for makercode, breakercode in zip(makercode.get_code(), breakercode.get_code()):
        if makercode == breakercode:
            dict_response["pegs_colourPosition"] += 1
        else:
            remaining_maker.append(makercode)
            remaining_breaker.append(breakercode)

    for breakercode in list(set(remaining_breaker)):  # Iter every different colour in the remaining breaker pattern
        dict_response["pegs_colour"] += remaining_maker.count(breakercode)

    return dict_response


def codemaker_winner(pegs_dict):
    """Return if the codebreaker has won giving the pegs dict"""

    return pegs_dict['pegs_colourPosition'] == len(PATTERN_COLOURS)
