from django.conf.urls import url, include
from rest_framework import routers

from CustomUser.views import CustomUserViewSet


router = routers.DefaultRouter()
router.register(r'user', CustomUserViewSet)


urlpatterns = [
    url(r'^', include(router.urls))
]
