from django.contrib.auth.models import User
from rest_framework import serializers


# Custom User serializer
class CustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')
