from rest_framework import viewsets
from django.contrib.auth.models import User

from CustomUser.serializers import CustomUserSerializer


class CustomUserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = CustomUserSerializer
    http_method_names = ['get']
