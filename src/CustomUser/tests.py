import random

from django.test import TestCase

from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.db.models import Count


random_user_name = "usertest_{}".format(hash(random.random()))


class UserTestCase(TestCase):

    def setUp(self):
        User.objects.create(
            username=random_user_name,
        )

    def test_cant_create_duplicated_users(self):
        """Do not allow create duplicated users"""
        try:
            User.objects.create(
                username=random_user_name,
            )
        except IntegrityError:
            pass
        else:
            self.fail("The object has been duplicated correctly")

    def test_duplicated_users(self):
        """Check if exists 2 or more users with the same name"""

        if User.objects.all().values('username').annotate(total=Count('username')).filter(total__gt=1).exists():
            self.fail("There are more than 1 user with the same username")
