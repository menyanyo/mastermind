This is the API REST basic structure of the Mastermind game.

Game concepts:
The codemaker generates a pattern of 4 colours (duplicated allowed). Afterwards,
the codebreaker tries to figure out the codemaker's code, proposing a pattern. 
The codemaker has to indicate with black pegs how many colours of the 
codebreaker's pattern are with right colour and right position either. White 
pegs are used to indicate how many colours of the pattern match with the colour 
but not with the right order.
More info: https://en.wikipedia.org/wiki/Mastermind_(board_game)

Structure of the project:
There are 3 applications in this django project:
- Mastermind: It's the base app of the project, that includes constants, 
settings, base urls and more.
- Customuser: It's the app that includes functions about users. For this program, 
we need only to know the username of the players.
- Pattern: It's the app where there are the functions related with the pattern 
of colours and more.
- Workflow: It's the app which contains the info about the workflow of the game,
as the mechanism to get the pegs.
- API: It contains the basic config and work around of the api that returns the
codemaker's response, given codebreaker's code.

Aims: Simulate the role of the Masterminds codemaker
- Create game (given a user request)
- Return feedback given a code guess

How to play?
1. Install the project, make the migrations and run the server:
- git clone "https://gitlab.com/menyanyo/mastermind.git"
- ./manage.py migrate
- ./manage.py runserver --insecure

The insecure param is only for visual purposes, to see the DRF static files 
without problems.

2. Go to in your browser: ^/api/v0/creategame   (you will see a default view
with the fields you need to give in the API REST CALL).

3. You can play against this codemaker in the url: ^/api/v0/attemptcodebreaker
Remember that no user can has more than 1 games opened at same time.

Extras:
- You can run the test in the app with: manage.py test
- You have do 3 extra request to the server:
1. ^/user
2. ^/game
3. ^/pattern

Author: Marc Bernardo Ferré
email: befemarc@hotmail.com
Last update: 24/06/2019
